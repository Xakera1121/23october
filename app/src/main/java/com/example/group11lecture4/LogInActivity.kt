package com.example.group11lecture4

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*


class LogInActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar!!.hide()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }


    private fun init() {
        auth = FirebaseAuth.getInstance()

        logInButton.setOnClickListener {
            if (emailEditText.text.isNotEmpty() && passwordEditText.text.isNotEmpty()) {
                log()
            } else {
                Toast.makeText(this, "Failed", Toast.LENGTH_LONG).show()
            }
        }
        signUpButton.setOnClickListener {
            saveUserData()
            val intent = Intent(this, ThirdActivity::class.java)
            intent.putExtra("email", emailEditText.text.toString())
            intent.putExtra("password", passwordEditText.text.toString())
            startActivity(intent)
        }
    }

    private fun log() {
        readData()
        auth.signInWithEmailAndPassword(
            emailEditText.text.toString(),
            passwordEditText.text.toString()
        )
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("log", "signInWithEmail:success")
                    val user = auth.currentUser

                    val intent = Intent(this, ContentActivity::class.java)
                    startActivity(intent)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("log", "signInWithEmail:failure", task.exception)
                    Toast.makeText(
                        baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                }


            }

    }

    private fun saveUserData() {
        val sharedPreferences = getSharedPreferences("user_data", Context.MODE_PRIVATE)
        val edit = sharedPreferences.edit()
        edit.putString("email",emailEditText.text.toString())
        edit.putString("password",passwordEditText.text.toString())
        edit.commit()

    }
    private fun readData(){
        val sharedPreferences = getSharedPreferences("user_data", Context.MODE_PRIVATE)
        emailEditText.setText(sharedPreferences.getString("email",""))
        passwordEditText.setText(sharedPreferences.getString("email",""))
    }

    private fun signup() {
        auth.createUserWithEmailAndPassword(
            emailEditText.text.toString(),
            passwordEditText.text.toString()
        )
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("signup", "createUserWithEmail:success")
                    val user = auth.currentUser
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("signup", "createUserWithEmail:failure", task.exception)
                    Toast.makeText(
                        baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                }

            }
    }

}


