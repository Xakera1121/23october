package com.example.group11lecture4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_third.*

private lateinit var auth: FirebaseAuth

class ThirdActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third)
        auth = FirebaseAuth.getInstance()
        init()
    }

    private fun init(){
        SignUpButton.setOnClickListener {
            reg()
            if (emailiEditText.text.isNotEmpty() && passwordiEditText.text.isNotEmpty()) {
                Toast.makeText(this, "Sign Up Success", Toast.LENGTH_LONG).show()
                val intent = Intent(this,LogInActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(this, "Sign Up Failed", Toast.LENGTH_LONG).show()
            }

        }
    }

    private fun reg(){
        auth.createUserWithEmailAndPassword(emailiEditText.text.toString(), passwordiEditText.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("signup", "createUserWithEmail:success")
                    val user = auth.currentUser
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("reg", "createUserWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }


            }
    }
}
