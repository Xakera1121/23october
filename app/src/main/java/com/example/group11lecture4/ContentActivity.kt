package com.example.group11lecture4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_content.*

class ContentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar!!.hide()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)
        init()
    }

    private fun init() {
        emailEditText.text= intent.extras?.getString("email","")
        passwordEditText.text= intent.extras?.getString("password","")

        Glide.with(this)
            .load("https://upload.wikimedia.org/wikipedia/commons/0/0f/20140321_Dancing_Stars_Conchita_Wurst_4187.jpg")
            .into(ImageView1)

    }


}
